<?php

use yii\db\Migration;

/**
 * Class m190330_231723_alter_routes_table
 */
class m190330_231723_alter_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('routes','zone_id',$this->integer()->comment('Зона'));
        $this->addColumn('routes','town',$this->string()->comment('Город'));
        $this->addColumn('routes','region',$this->string()->comment('Район'));
        $this->createIndex('idx-rotes-zone_id','routes','zone_id');
        $this->addForeignKey('fk-rotes-zone_id','routes','zone_id','zones','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190330_231723_alter_routes_table cannot be reverted.\n";

        return false;
    }
    */
}
