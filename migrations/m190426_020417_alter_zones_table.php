<?php

use yii\db\Migration;

/**
 * Class m190430_120417_alter_zones_table
 */
class m190426_020417_alter_zones_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('zones','count_entrance',$this->integer()->comment('Количество подьездов'));
        $this->addColumn('zones','count_floor',$this->integer()->comment('Количество етажей'));
        $this->addColumn('zones','count_apartament',$this->integer()->comment('Количество квартир'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }


}
