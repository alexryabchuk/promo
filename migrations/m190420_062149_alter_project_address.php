<?php

use yii\db\Migration;

/**
 * Class m190420_062149_alter_project_address
 */
class m190420_062149_alter_project_address extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('project_address','coord_x',$this->decimal(16,8));
        $this->addColumn('project_address','coord_y',$this->decimal(16,8));

        $this->createIndex('idx-coord_x-project_address','project_address','coord_x');
        $this->createIndex('idx-coord_y-project_address','project_address','coord_y');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190420_062149_alter_project_address cannot be reverted.\n";

        return false;
    }
    */
}
