<?php

use yii\db\Migration;

/**
 * Class m190406_181109_alter_projects_table
 */
class m190406_181109_alter_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('projects','circulation_fact',$this->integer()->comment('Фактический тираж'));
        $this->addColumn('projects','normal_one',$this->integer()->comment('Норма на одного'));
        $this->addColumn('projects','maket',$this->string()->comment('Макет'));
        $this->addColumn('projects','maket_file',$this->string()->comment('Файл макета'));
        $this->addColumn('projects','info_add',$this->string()->comment('Доаполнительная информация'));
        $this->addColumn('projects','route_template_id',$this->integer()->comment('Шаблон маршрутного листа'));
        $this->createIndex('idx-route_template_id','projects','route_template_id');
     }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190406_181109_alter_projects_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190406_181109_alter_projects_table cannot be reverted.\n";

        return false;
    }
    */
}
