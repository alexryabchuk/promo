<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employees_move}}`.
 */
class m190414_212927_create_employees_move_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employees_move}}', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->comment('Промоутер'),
            'route_id' => $this->integer()->comment('Маршрут'),
            'dateandtime' => $this->dateTime()->comment('Дата и время'),
            'coord_x' => $this->decimal(10,8)->comment('X'),
            'coord_y' => $this->decimal(10,8)->comment('Y'),
            'status' => $this->smallInteger()->comment('Статус'),
        ]);
        $this->createIndex('idx-employee_id-employees_move','employees_move','employee_id');
        $this->createIndex('idx-route_id-employees_move','employees_move','route_id');
        $this->createIndex('idx-dateandtime-employees_move','employees_move','dateandtime');
        $this->addForeignKey('fk--employee_id-employees_move','employees_move','employee_id','employees','id');
        $this->addForeignKey('fk--route_id-employees_move','employees_move','route_id','routes','id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk--employee_id-employees_move','employees_move');
        $this->dropForeignKey('idx-route_id-employees_move','employees_move');
        $this->dropTable('{{%employees_move}}');
    }
}
