<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%project_address}}`.
 */
class m190330_141051_create_project_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%project_address}}', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->comment('Проект'),
            'address_id' => $this->integer()->comment('Адрес'),
            'zone_id' => $this->integer()->comment('Зона'),
            'entrance' => $this->integer()->comment('К-во подъездов'),
            'apartament' => $this->integer()->comment('К-во квартир'),
            'floor' => $this->integer()->comment('К-во этажей'),
            'porter' => $this->integer()->comment('К-во вахт'),
            'type' => $this->integer()->comment('Тип'),
            'comment' => $this->integer()->comment('Комментарий')
         ]);
        $this->createIndex('idx-project_address-project_id','project_address','project_id',false);
        $this->addForeignKey('fk-project_address-project_id','project_address','project_id','projects','id');
        $this->createIndex('idx-project_address-address_id','project_address','address_id',false);
        $this->addForeignKey('fk-project_address-address_id','project_address','address_id','address_list','id');
        $this->createIndex('idx-project_address-zone_id','project_address','zone_id',false);
        $this->addForeignKey('fk-project_address-zone_id','project_address','zone_id','zones','id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%project_address}}');
    }
}
