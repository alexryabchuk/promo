<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%comments}}`.
 */
class m190409_065214_create_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%comments}}', [
            'id' => $this->primaryKey(),
            'comment_date' => $this->dateTime()->comment(''),
            'message' => $this->string()->comment(''),
            'user_id' => $this->integer()->comment(''),
            'user_to' => $this->integer()->comment(''),
            'confirm' => $this->smallInteger()->comment(''),
            'route_id' => $this->integer()->comment(''),
        ]);
        $this->createIndex('idx-user_id','{{%comments}}','user_id');
        $this->createIndex('idx-user_to','{{%comments}}','user_to');
        $this->createIndex('idx-route_id','{{%comments}}','route_id');
        $this->addForeignKey('fk-comments-user_id','{{%comments}}','user_id','users','id');
        $this->addForeignKey('fk-comments-user_to','{{%comments}}','user_to','users','id');
        $this->addForeignKey('fk-comments-route_id','{{%comments}}','route_id','routes','id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-comments-user_id','{{%comments}}');
        $this->dropForeignKey('fk-comments-user_to','{{%comments}}');
        $this->dropForeignKey('fk-comments-route_id','{{%comments}}');
        $this->dropTable('{{%comments}}');
    }
}
