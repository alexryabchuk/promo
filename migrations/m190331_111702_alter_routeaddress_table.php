<?php

use yii\db\Migration;

/**
 * Class m190331_111702_alter_routeaddress_table
 */
class m190331_111702_alter_routeaddress_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('route_address','comment',$this->string()->comment('Комментарий'));
        $this->addColumn('route_address','plane_date',$this->date()->comment('Плановая дата'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('route_address','comment');
        $this->dropColumn('route_address','plane_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190331_111702_alter_routeaddress_table cannot be reverted.\n";

        return false;
    }
    */
}
