<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%zones}}`.
 */
class m190330_061054_create_zones_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%zones}}', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->comment('Проект'),
            'num_zone' => $this->integer()->comment('№ зоны'),
            'name'=>$this->string('60')->comment('Название'),
            'coorarr'=>$this->text()->comment('Коорданаты'),
            'border'=>$this->string()->comment('Границы'),
            'objects_count'=>$this->integer()->comment('Кол-во объектов')

        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%zones}}');
    }
}
