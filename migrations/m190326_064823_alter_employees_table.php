<?php

use yii\db\Migration;

/**
 * Class m190326_064823_alter_employees_table
 */
class m190326_064823_alter_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('employees','account',$this->smallInteger(1));
        $this->addColumn('employees','login',$this->string(32));
        $this->addColumn('employees','password',$this->string(32));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('employees','login');
        $this->dropColumn('employees','password');
        $this->addColumn('employees','account');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190326_064823_alter_employees_table cannot be reverted.\n";

        return false;
    }
    */
}
