<?php

use yii\db\Migration;

/**
 * Class m190404_100704_alter_routes_table
 */
class m190404_100704_alter_routes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('routes','placement_id', $this->integer()->comment('Метод'));
        $this->addColumn('routes','maket', $this->string()->comment('Макет'));
        $this->addColumn('routes','count_apartament', $this->integer()->comment('Количество квартир'));
        $this->addColumn('routes','count_entrance', $this->integer()->comment('Количество подъездов'));
        $this->createIndex('idx-routes-placement_id', '{{%routes}}', 'placement_id', false);
        $this->addForeignKey("fk-routes-placement_id", "{{%routes}}", "placement_id", "placement_method", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk-routes-placement_id", "{{%routes}}");
        $this->dropColumn('routes','placement_id');
        $this->dropColumn('routes','maket');
        $this->dropColumn('routes','count_apartament');
        $this->dropColumn('routes','count_entrance');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190404_100704_alter_routes_table cannot be reverted.\n";

        return false;
    }
    */
}
