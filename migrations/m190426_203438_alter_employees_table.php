<?php

use yii\db\Migration;

/**
 * Class m190426_203438_alter_employees_table
 */
class m190426_203438_alter_employees_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('employees','status_work',$this->smallInteger()->comment('Статус работ'));
        $this->addColumn('employees','coord_x',$this->decimal(16,8)->comment('Координата X'));
        $this->addColumn('employees','coord_y',$this->decimal(16,8)->comment('Координата Y'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {



    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190426_203438_alter_employees_table cannot be reverted.\n";

        return false;
    }
    */
}
