﻿<pre style="word-wrap: break-word; white-space: pre-wrap;">-------------------------------------------------------------------
-----------------API приложения------------------------------------
ключ для тестирования b9EqBtQJSQkFNdeJ8K4uicIOh2EXuoir
-------------------------------------------------------------------
Список всех компаний - GET
 http://http://basket.teo-crm.ru/api/client/get-all-company

Принимает
access - ключ

Возвращает 
{
    "status":true,                       //
    "errors":null,                       //
    "companyList":{                      //
        'id' : ['id'],                   // id компании
        'name' :['name'],                // Название компании
        'description' :['description'],  // Описание компании
        'picture' :['description'],      // Изображение компании
        'coord_x' :['coord_x'],          // Координата X
        'coord_y' :['coord_y'],          // Координата Y
        
    }
}

-------------------------------------------------------------------
Список всех категорий компаний - GET
 http://http://basket.teo-crm.ru/api/client/get-category-company

Принимает
access - ключ

Возвращает 
{
    "status":true,
    "errors":null,
    "categoryCompanyList":{
        'id' : ['id'],                   // id категории
        'name' :['name'],                // Название категории
    }
}

-------------------------------------------------------------------
Список всех компаний в категории - GET
 http://http://basket.teo-crm.ru/api/client/get-company-by-category

Принимает
access - ключ
category_id - id категории

Возвращает 
{
    "status":true,
    "errors":null,
    "companyList":{                      //
        'id' : ['id'],                   // id компании
        'name' :['name'],                // Название компании
        'description' :['description'],  // Описание компании
        'picture' :['description'],      // Изображение компании
        'coord_x' :['coord_x'],          // Координата X
        'coord_y' :['coord_y'],          // Координата Y
        
    }
}

-------------------------------------------------------------------
Список всех категорий товаров - GET
 http://http://basket.teo-crm.ru/api/client/get-category-product

Принимает
access - ключ

Возвращает 
{
    "status":true,
    "errors":null,
    "categoryProductList":{
        'id' : ['id'],                   // id категории
        'name' :['name'],                // Название категории
    }
}

-------------------------------------------------------------------
Список всех товаров - GET
 http://http://basket.teo-crm.ru/api/client/get-all-product

Принимает
access - ключ

Возвращает 
{
    "status":true,
    "errors":null,
    "productList":{
        'id' : ['id'],                   // id компании
        'name' :['name'],                // Название товара
        'description' :['description'],  // Описание товара
        'picture' : ['picture'],         // рисунок
        'weight' :['weight'],            // вес
        'price' :['price'],              // Цена
    }
}

-------------------------------------------------------------------
Список всех товаров в категории - GET
 http://http://basket.teo-crm.ru/api/client/get-product-by-category

Принимает
access - ключ
category_id - id категории

Возвращает 
{
    "status":true,
    "errors":null,
    "productList":{
        'id' : ['id'],                   // id компании
        'name' :['name'],                // Название товара
        'description' :['description'],  // Описание товара
        'picture' : ['picture'],         // рисунок
        'weight' :['weight'],            // вес
        'price' :['price'],              // Цена
    }
}

