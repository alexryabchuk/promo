var lang = "ru-RU";
var year = 2018;

function dateToTS (date) {
    return date.valueOf();
}

function tsToDate (ts) {
    var d = new Date(ts);

    return d.toLocaleTimeString(lang, {
        hour: 'numeric',
        minute: 'numeric',
    });
}


var myMap = null;
var myMap2 = null;
var array = null;
var myCollection = null;
var objectCollection = null;



function getEmployeeRoute() {


    $.get("/online-map/get-employee-last-place",{type:0},function (data) {
        PlaceEmployees(data);
        console.log(data);
    })
}

function PlaceEmployees(arr) {

    for (step = 0; step < arr.length; step++){
        txtycoor = [arr[step]['coord_x'],arr[step]['coord_y']];
        var myPlacemark = new ymaps.Placemark(txtycoor,{
            hintContent: arr[step]['employee_id'],
        }, {
            preset: 'islands#circleDotIcon',
            iconLayout: 'default#image',

            iconImageHref: '/images/yandex/promoyt'+arr[step]['status']+'.png',

            iconImageSize: [30, 42],
            iconColor: '#0095b6',

        });
        objectCollection.add(myPlacemark);
        myMap.geoObjects.add(objectCollection);
    }

};

function init () {
    $("#example_id").ionRangeSlider({
        skin: "big",

        grid: true,
        min: dateToTS(new Date(year, 10, 1,0,0)),
        max: dateToTS(new Date(year, 10, 1,23,59)),
        from: dateToTS(new Date(year, 10, 1,8,0)),
        prettify: tsToDate
    });
    //myCollection = new ymaps.GeoObjectCollection();
    myCollection = new ymaps.GeoObjectCollection();
    objectCollection = new ymaps.GeoObjectCollection();
    // Создаем карту с добавленными на нее кнопками.
    if (myMap) myMap.destroy();
    myMap = new ymaps.Map('map', {
        center: center_map,
        zoom: 12,
    }, {
        buttonMaxWidth: 300
    });
    myMap2 = new ymaps.Map('map2', {
        center: center_map,
        zoom: 12,
    }, {
        buttonMaxWidth: 300
    });
    getEmployeeRoute();
    // var verticles = array.map(function (point) {
    //     return [point.coord_x,point.coord_y]}),
    // line = new ymaps.Polyline(verticles);
    // myMap.geoObjects.add(line);
    // array.forEach(function(item, i, array) {
    //     myMap.geoObjects
    //         .add(new ymaps.Placemark([item.coord_x, item.coord_y], {
    //             balloonContent: item.id,
    //             iconContent: item.id,
    //             hintContent: item.id
    //         }, {
    //             preset: 'islands#circleDotIcon',
    //             iconColor: '#0095b6',
    //             iconImageSize: [2, 2],
    //         }));
    // });
}
ymaps.ready(init);
