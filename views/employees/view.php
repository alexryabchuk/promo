<?php
use Yii,
    yii\helpers\Html,
    yii\helpers\Url,
    yii\bootstrap\Tabs;


$this->title = 'Сотрудник:'.$model->FIO;

$this->params['breadcrumbs'][] = ['label' => 'Справочник сотрудников','url'=>['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?php
echo Tabs::widget([
    'items' => [
        [
            'label' => 'Информация о сотруднике',
            'content' => $this->render('_view_main', [
                'model' => $model,
                'attachPassport' => $attachPassport,
                'attachOther' => $attachOther,
            ]),
            'active' => true,
        ],
        [
            'label' => 'Маршруты',
            'content' => $this->render('_view_route', [
                'routeModel' => $routeModel,
                'model' => $model,
            ]),
        ],
        [
            'label' => 'История статусов',
            'content' => $this->render('_view_status', [
                'model' => $model,
                'statuses' => $statuses,

            ]),
        ],
    ]
]);

?>


