
<div class="projects-form">
    <div class="row">
        <table class="table table-bordered " cellpadding="0" cellspacing="0" style="border: 1px solid black">
            <thead>
                <tr>
                    <th class="text-center" style="border: 1px solid black">
                        Район
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Улица
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Дом
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Строение
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Количество подъездов
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Количество этажей
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Количество квартир
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Зона
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        Фото
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        1п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        2п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        3п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        4п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        5п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        6п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        7п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        8п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        9п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        10п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        11п
                    </th>
                    <th class="text-center" style="border: 1px solid black">
                        12п
                    </th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($address as $item) :?>
            <tr>
                <td style="border: 1px solid black">
                    <?=$model->region?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->street?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->house?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->housing?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->entrance?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->floor?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->apartament?>
                </td>
                <td style="border: 1px solid black">
                    <?=$model->zone->num_zone?>
                </td>
                <td style="border: 1px solid black">
                    <?=$item->address->entrance*4+1?>
                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
                <td style="border: 1px solid black">

                </td>
            </tr>
            <?php endforeach;?>
            </tbody>

        </table>
    </div>
</div>
