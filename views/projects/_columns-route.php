<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'project.name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'project.client.name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Тип работ',
        'attribute'=>'placementMethod.name',

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Макет',
        'attribute'=>'maket',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'town',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'region',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'status_route',
//        'value'=>'CurrentStatus',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'name',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'normal',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Зона',
        'attribute'=>'zone.name',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'start_date',
//        'value' => function($model){
//            return $model->start_date  ? Yii::$app->formatter->asDatetime($model->start_date) : Html::a('Запланировать',['set-plan','id'=>$model->id],['role'=>'modal-remote']);
//        },
//        'format'=>['date', 'php:d.m.Y'],
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'fact_date',
//        'format'=>['date', 'php:d.m.Y'],
//    ],

    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'category_id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'employees_id',
//        'value' => function($model){
//            switch ($model->status_route) {
//                case 0 : return '';
//                case 1: return Html::a('Назначить',['set-employee','id'=>$model->id],['role'=>'modal-remote']);;
//                case 2: return $model->employees->FIO;
//                case 3: return $model->employees->FIO.Html::a('<i class="glyphicon glyphicon-share"></i>',['duplicate-route','id'=>$model->id],['role'=>'modal-remote','class'=>'pull-right']);;;
//                default: return '';
//            }
//        },
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'comment_employees',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment_manager',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'duplicated',
//        'visible' => false,
//    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'count_adress',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'fact_count_adress',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'header' =>"&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp",
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{print} {print-address} {road} {update} {delete}',
        'buttons' => [
            'road' => function($url, $model, $key) {     // render your custom button
                return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-road"></span></button>',
                    ['view-route','route_id'=>$model->id],['data-pjax' => '0','title'=>'Адреса']);
            },
            'print' => function($url, $model, $key) {     // render your custom button
                return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-print"></span></button>',
                ['print-route','route_id'=>$model->id],['title'=>'Печать','target' => '_blank','data-pjax'=>"0"]);
            },
            'print-address' => function($url, $model, $key) {     // render your custom button
                return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-print"></span></button>',
                ['print-route-address','route_id'=>$model->id],['title'=>'Печать','target' => '_blank','data-pjax'=>"0"]);
}
        ],
        'urlCreator' => function($action, $model, $key, $index) {

            if($action == 'view'){
                return Url::to(['view','id'=>$model->id]);
            }

            return Url::to([$action.'-route','route_id'=>$model->id]);
        },
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','data-pjax' => 0],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','title'=>'Изменить', 'role'=>'modal-remote', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверенны?',
            'data-confirm-message'=>'Вы действительно хотите удалить запись '],
    ],

];