<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Projects */

?>
<div class="projects-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
