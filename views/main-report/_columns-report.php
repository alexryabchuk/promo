<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => '№ записи',
        'attribute'=>'id',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Клиент',
        'attribute'=>'clientname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => '№ проекта',
        'attribute'=>'projectid',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Проект',
        'attribute'=>'projectname',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Способ размещения',
        'attribute'=>'placementmethod',
    ],
    [
        'headerOptions' => [
            'colspan' => '2',
            'style' => 'padding:0px; width:120px',

        ],
        'header' => "<table style = 'width: 120px;' ><tr style='border-bottom: 1px solid #ddd'><th colspan=2 style='text-align: center'>Тираж</th></tr>".
                    "<tr><th style = 'width:59px ;border-right: 1px solid #ddd; text-align: center'>Норма</th><th style='text-align: center'>Факт</th></tr></table>",
        'class'=>'\kartik\grid\DataColumn',
        'label' => '<tr>vaxvfsad</tr>',
        'attribute'=>'circulation',
        'options' => ['style' => 'width: 60px; max-width: 60px;'],

    ],
    [
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Тираж',
        'attribute'=>'factcirculation',
        'contentOptions' => ['style' => 'width: 60px; max-width: 60px;'],
    ],

    [
        'headerOptions' => [
            'colspan' => '2',
            'style' => 'padding:0px; width:1920px',
        ],
        'header' => "<table style = 'width: 192px;' ><tr style='border-bottom: 1px solid #ddd'><th colspan=2 style='text-align: center'>Период</th></tr>".
            "<tr><th style = 'width:95px ;border-right: 1px solid #ddd; text-align: center'>Норма</th><th style='text-align: center'>Факт</th></tr></table>",
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'routeperiod',
        'options' => ['style' => 'width: 96px;'],
    ],
    [
        'headerOptions' => [
            'style' => 'display: none;',
        ],
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'routeperiodfact',
        'contentOptions' => ['style' => 'width: 96px;'],
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'town',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'region',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Промоутер',
        'attribute'=>'employees',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' =>"&nbsp&nbsp&nbspДействия&nbsp&nbsp&nbsp",
        'template' => '{report}',
        'buttons' => [
            'report' => function($url, $model, $key) {     // render your custom button
                    return Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-dashboard"></span></button>',
                        ['confirm-work', 'route_id' => $model['id']], ['data-pjax' => '0', 'title' => 'Адреса']);
            },
        ],
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$model['id']]);
        },
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверенны?',
            'data-confirm-message'=>'Вы действительно хотите удалить запись '],
],

];   