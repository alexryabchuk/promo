<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RouteTemplates */
?>
<div class="route-templates-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'template:ntext',
        ],
    ]) ?>

</div>
