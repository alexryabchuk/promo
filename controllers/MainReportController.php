<?php

namespace app\controllers;

use app\models\RouteAddress;
use app\models\Routes;
use app\models\search\RouteAddressSearch;
use app\models\search\RoutesSearch;
use Yii;
use app\models\Fines;
use app\models\FinesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * FinesController implements the CRUD actions for Fines model.
 */
class MainReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Fines models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModelPlaneWork = new RoutesSearch();
        $dataProviderPlaneWork = $searchModelPlaneWork->searchReportPlaneWork(Yii::$app->request->queryParams);
        $searchModelAssignedGrafik = new RoutesSearch();
        $dataProviderAssignedGrafik = $searchModelAssignedGrafik->searchReportAssignedGrafik(Yii::$app->request->queryParams);
        $searchModelReportFact = new RoutesSearch();
        $dataProviderReportFact = $searchModelReportFact->searchReportFact(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModelPlaneWork' => $searchModelPlaneWork,
            'dataProviderPlaneWork' => $dataProviderPlaneWork,
            'searchModelAssignedGrafik' => $searchModelAssignedGrafik,
            'dataProviderAssignedGrafik' => $dataProviderAssignedGrafik,
            'searchModelReportFact' => $searchModelReportFact,
            'dataProviderReportFact' => $dataProviderReportFact,
        ]);
    }


    /**
     * Displays a single Fines model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Fines #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Fines model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Fines();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Fines",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new Fines",
                    'content'=>'<span class="text-success">Create Fines success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new Fines",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Fines model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRouteEdit($id)
    {
        $request = Yii::$app->request;
        $model = Routes::findOne($id);
        $searchModelAddress= new RouteAddressSearch();
        $dataProviderAddress  = $searchModelAddress->search(Yii::$app->request->queryParams);
        $dataProviderAddress->query->andWhere(['route_id' => $id]);
        if($model->load($request->post()) && $model->save()){
            return $this->goBack();
        } else {
                return $this->render('route-edit', [
                    'model' => $model,
                    'dataProviderAddress' => $dataProviderAddress,
                    'searchModelAddress' => $searchModelAddress,
                ]);
        }
    }

    public function actionAssignEmployee($route_id)
    {
        $request = Yii::$app->request;
        $model = Routes::findOne($route_id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Назначить промоутера",
                    'content'=>$this->renderAjax('select-employee', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Выбрать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())) {

                $model->save();
                return [
                    'forceReload' => '#crud-assigned-grafik-pjax',
                    'forceClose'=>true,
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['/main-report']);
            } else {
                return $this->render('select-employee', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionSendEmployee($route_id)
    {
        $request = Yii::$app->request;
        $model = Routes::findOne($route_id);
        $model->status_route = Routes::ROUTE_PLAN;
        $model->save();
        //TODO отправка маршрута в телефон
        if($request->isAjax){
            return [
                'forceReload' => '#crud-assigned-grafik-pjax',
            ];
        }else{
            return $this->redirect(['/main-report']);
        }
    }

    public function actionCommentEmployee($route_id)
    {
        $request = Yii::$app->request;
        $model = Routes::findOne($route_id);
        $model->comment_employees = preg_replace("'<b[^>]*?>.*?</b>'si","",$model->comment_employees);
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){

                return [
                    'title'=> "Комментарий",
                    'content'=>$this->renderAjax('comment-employee', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Выбрать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())) {
                $model->comment_employees = "<b>".Yii::$app->formatter->asDate(time(),'dd.MM.y H:m')."</b> ".trim($model->comment_employees);
                $model->save();
                return [
                    'forceReload' => '#crud-assigned-grafik-pjax',
                    'forceClose'=>true,
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['/main-report']);
            } else {
                return $this->render('comment-employee', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionCommentManager($route_id)
    {
        $request = Yii::$app->request;
        $model = Routes::findOne($route_id);
        $model->comment_employees = preg_replace("'<b[^>]*?>.*?</b>'si","",$model->comment_employees);
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){

                return [
                    'title'=> "Комментарий",
                    'content'=>$this->renderAjax('comment-manager', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Выбрать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())) {
                $model->comment_employees = "<b>".Yii::$app->formatter->asDate(time(),'dd.MM.y H:m')."</b> ".trim($model->comment_employees);
                $model->save();
                return [
                    'forceReload' => '#crud-assigned-grafik-pjax',
                    'forceClose'=>true,
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['/main-report']);
            } else {
                return $this->render('comment-manager', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionConfirmWork($route_id)
    {
        $request = Yii::$app->request;
        $routeAddress = RouteAddress::find()->where(['route_id'=>$route_id])->all();
        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'title'=> "Комментарий",
                    'content'=>$this->renderAjax('confirm-work', [
                        'model' => $routeAddress,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Выбрать',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else {
                return $this->render('confirm-work', [
                    'model' => $routeAddress,
                ]);
            }

    }

    /**
     * Delete an existing Fines model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Fines model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Fines model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Fines the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Fines::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
