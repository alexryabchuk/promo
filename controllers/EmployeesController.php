<?php

namespace app\controllers;

use app\models\EmployeesAttach;
use app\models\EmployeesChildren;
use app\models\EmployeesMove;
use app\models\EmployeesStatus;
use app\models\forms\AttachForm;
use app\models\forms\RoutePerDateForm;
use app\models\RouteAddress;
use Yii;
use app\models\Employees;
use app\models\search\EmployeesSearch;
use yii\base\Security;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * EmployeesController implements the CRUD actions for Employees model.
 */
class EmployeesController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),

                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
//                        'matchCallback' => function ($rule, $action) {
//                            if (!Yii::$app->user->identity->isSuperAdmin()) {
//                                return $this->redirect(['/']);
//                            }
//                            return true;
//
//                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {    
        $searchModel = new EmployeesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $routeModel = new RoutePerDateForm();
        $attachPassport = EmployeesAttach::find()->where(['employees_id'=>$id,'category_file'=>1])->all();
        $attachOther = EmployeesAttach::find()->where(['employees_id'=>$id,'category_file'=>2])->all();
        $children = EmployeesChildren::find()->where(['employees_id'=>$id])->all();
        $statuses = EmployeesStatus::find()->where(['employees_id'=>$id])->all();
            return $this->render('view', [
                'model' => $this->findModel($id),
                'attachPassport' => $attachPassport,
                'attachOther' => $attachOther,
                'routeModel' => $routeModel,
                'children' => $children,
                'statuses' => $statuses,
            ]);
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Employees();
            if ($model->load($request->post()) && $model->save()) {
                $model->main_photo = UploadedFile::getInstance($model, 'main_photo');
                $main_path = "images/photo/";
                if (!empty($model->main_photo)) {
                    if (!file_exists(($main_path))) {
                        mkdir($main_path, 0777, true);
                    }
                    $main_photo = $main_path . time() . Yii::$app->security->generateRandomString(5) . '.' . $model->main_photo->extension;
                    $model->main_photo->saveAs($main_photo);
                    $model->foto = '/'.$main_photo;
                    $model->save();
                }
                //загрузка паспортов .
                $model->saveAttachPassport();
                //загрузка других .
                $model->saveAttachOther();
                //Сохранить список детей
                $model->children = $request->post('Employees')['children'];
                $model->saveChildren();
                return $this->redirect(['index', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $queryPassport = EmployeesAttach::find()->where(['employees_id'=>$id,'category_file'=>1]);
        $dataProviderPassport = new ActiveDataProvider(['query' => $queryPassport]);
        $queryOther = EmployeesAttach::find()->where(['employees_id'=>$id,'category_file'=>2]);
        $dataProviderOther = new ActiveDataProvider(['query' => $queryOther]);
        $model->children = EmployeesChildren::find()->where(['employees_id'=>$id])->asArray()->all();

        if ($model->load($request->post()) && $model->save()) {
            $model->main_photo = UploadedFile::getInstance($model, 'main_photo');
            $main_path = "images/photo/";
            if (!empty($model->main_photo)) {
                if (!file_exists(($main_path))) {
                    mkdir($main_path, 0777, true);
                }
                $main_photo = $main_path . time() . Yii::$app->security->generateRandomString(5) . '.' . $model->main_photo->extension;
                $model->main_photo->saveAs($main_photo);
                $model->foto = '/'.$main_photo;
                $model->save();
            }

            //загрузка паспортов .
            $model->saveAttachPassport();
            //загрузка других .
            $model->saveAttachOther();
            //Сохранить список детей
            $model->children = $request->post('Employees')['children'];
            $model->saveChildren();

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProviderPassport' => $dataProviderPassport,
                'dataProviderOther' => $dataProviderOther,

            ]);
            }
    }

    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }


    }

    public function actionAttachDelete($category, $id)
    {
        $request = Yii::$app->request;
        EmployeesAttach::findOne($id)->delete();
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($category ==1) {
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-passport-pjax'];
            } else {
                return ['forceClose'=>true,'forceReload'=>'#crud-datatable-other-pjax'];
            }

        }else{
            return $this->redirect(['index']);
        }


    }

    public function actionAttachAdd($category,$id)
    {
        $request = Yii::$app->request;
        $model = new AttachForm();
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавить вложения",
                    'content' => $this->renderAjax('_form-attach', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            }else if($model->load($request->post())){
                $model->saveAttach($category,$id);
                if ($category ==1) {
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-passport-pjax'];
                } else {
                    return ['forceClose'=>true,'forceReload'=>'#crud-datatable-other-pjax'];
                }
            }else{
                return [
                    'title' => "Добавить вложения",
                    'content' => $this->renderAjax('_form-attach', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                return $this->render('_form-attach', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    public function actionGetMoveRoute()
    {
        $request = Yii::$app->request;
        //if($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $route_id = intval($request->get('route'));
            $r = RouteAddress::getRouteAddress($route_id);
            return ['movies' => EmployeesMove::getEmployeeRoute($request->get('employeeId'),$route_id),
                'address'=>$r['address'],'center'=>$r['center']];
        //}
    }

    public function actionGetRoutesFromPeriod($employee,$startDate,$endDate)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Employees::getRouteFromPeriod($employee,$startDate,$endDate);
    }

    protected function findModel($id)
    {
        if (($model = Employees::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
