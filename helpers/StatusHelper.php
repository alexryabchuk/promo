<?php
/**
 * Created by PhpStorm.
 * User: Максим
 * Date: 20.02.2019
 * Time: 20:21
 */

namespace app\helpers;


class StatusHelper
{
    public static function listYesNo()
    {
        return [0=>'Нет',1=>'Да'];
    }

    public static function iconYesNo($value)
    {
        return $value ? '<i style="color:green" class="glyphicon glyphicon-ok"></i>' : '<i style="color:red" class="glyphicon glyphicon-remove"></i>';
    }

}