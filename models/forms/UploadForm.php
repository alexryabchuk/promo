<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadForm
 * @package app\models\forms
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @param $tmp_name
     * @return bool
     */
    public function upload($tmp_name)
    {
        Yii::info('this UploadForm', 'test');
        if ($this->validate()) {
            if (!file_exists('uploads')){
                mkdir('uploads');
            }
            if (!file_exists('uploads/excel')){
                mkdir('uploads/excel');
            }
            $this->file->saveAs('uploads/excel/' . $tmp_name . '.' . $this->file->extension);
            return true;
        } else {
            Yii::error($this->errors, 'error');
            return false;
        }
    }
}