<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProjectAddress;

/**
 * ProjectAddressSearch represents the model behind the search form about `app\models\ProjectAddress`.
 */
class ProjectAddressSearch extends ProjectAddress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'project_id', 'address_id', 'zone_id', 'entrance', 'apartament', 'floor', 'porter', 'type', 'comment'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProjectAddress::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'project_id' => $this->project_id,
            'address_id' => $this->address_id,
            'zone_id' => $this->zone_id,
            'entrance' => $this->entrance,
            'apartament' => $this->apartament,
            'floor' => $this->floor,
            'porter' => $this->porter,
            'type' => $this->type,
            'comment' => $this->comment,
        ]);

        return $dataProvider;
    }
}
