<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Employees;

/**
 * EmployeesSearch represents the model behind the search form about `app\models\Employees`.
 */
class EmployeesSearch extends Employees
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'manager_id', 'status', 'category_id', 'family_status', 'position_id'], 'integer'],
            [['firstname', 'lastname', 'parentname', 'birthday', 'address', 'phone', 'phone_add','FIO_guarantor','phone_model','phone_id','specialization','other_data', 'email', 'start_work', 'pasport_number', 'pasport_publish', 'pasport_date', 'code_structure', 'foto'], 'safe'],


        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employees::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'manager_id' => $this->manager_id,
            'status' => $this->status,
            'category_id' => $this->category_id,
            'birthday' => $this->birthday,
            'family_status' => $this->family_status,
            'position_id' => $this->position_id,
            'start_work' => $this->start_work,
            'pasport_date' => $this->pasport_date,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'parentname', $this->parentname])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'phone_add', $this->phone_add])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'pasport_number', $this->pasport_number])
            ->andFilterWhere(['like', 'pasport_publish', $this->pasport_publish])
            ->andFilterWhere(['like', 'code_structure', $this->code_structure])
            ->andFilterWhere(['like', 'foto', $this->foto]);

        return $dataProvider;
    }
}
