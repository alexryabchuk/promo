<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Routes;
use yii\db\Query;

/**
 * RoutesSearch represents the model behind the search form about `app\models\Routes`.
 */
class RoutesSearch extends Routes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_route', 'duplicated', 'category_id', 'employees_id', 'project_id', 'count_adress', 'fact_count_adress'], 'integer'],
            [['start_date', 'fact_date', 'video', 'comment_employees', 'comment_manager'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Routes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status_route' => $this->status_route,
            'start_date' => $this->start_date,
            'fact_date' => $this->fact_date,
            'duplicated' => $this->duplicated,
            'category_id' => $this->category_id,
            'employees_id' => $this->employees_id,
            'project_id' => $this->project_id,
            'count_adress' => $this->count_adress,
            'fact_count_adress' => $this->fact_count_adress,
        ]);

        $query->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'comment_employees', $this->comment_employees])
            ->andFilterWhere(['like', 'comment_manager', $this->comment_manager]);

        return $dataProvider;
    }

    public function searchReportPlaneWork($params)
    {
        $query = (new Query())
            ->select(
                [
                    'routes.id as id',
                    'routes.town as town',
                    'routes.region as region',
                    'routes.status_route as status',
                    'CONCAT(routes.plane_start_date,"-",routes.plane_end_date) as routeperiod',
                    'projects.name as projectname',
                    'projects.id as projectid',
                    'clients.name as clientname',
                    'placement_method.name as placementmethod',
                    'routes.normal as circulation'

                ]
            )
            ->from('routes')
            ->leftJoin('projects','projects.id=routes.project_id')
            ->leftJoin('placement_method','placement_method.id = routes.placement_id')
            ->leftJoin('clients','clients.id = projects.client_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status_route' => $this->status_route,
            'start_date' => $this->start_date,
            'fact_date' => $this->fact_date,
            'duplicated' => $this->duplicated,
            'category_id' => $this->category_id,
            'employees_id' => $this->employees_id,
            'project_id' => $this->project_id,
            'count_adress' => $this->count_adress,
            'fact_count_adress' => $this->fact_count_adress,
        ]);

        $query->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'comment_employees', $this->comment_employees])
            ->andFilterWhere(['like', 'comment_manager', $this->comment_manager]);

        return $dataProvider;
    }

    public function searchReportAssignedGrafik($params)
    {
        $query = (new Query())
            ->select(
                [
                    'routes.id as id',
                    'routes.town as town',
                    'routes.region as region',
                    'routes.status_route as status',
                    'routes.comment_employees as comment_employees',
                    'routes.comment_manager as comment_manager',
                    'routes.employees_id as employees_id',
                    'CONCAT(routes.plane_start_date,"-",routes.plane_end_date) as routeperiod',
                    'projects.name as projectname',
                    'projects.id as projectid',
                    'clients.name as clientname',
                    'placement_method.name as placementmethod',
                    'routes.normal as circulation',
                    'employees.firstname as employees',



                ]
            )
            ->from('routes')
            ->leftJoin('projects','projects.id=routes.project_id')
            ->leftJoin('employees','employees.id=routes.employees_id')
            ->leftJoin('placement_method','placement_method.id = routes.placement_id')
            ->leftJoin('clients','clients.id = projects.client_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status_route' => $this->status_route,
            'start_date' => $this->start_date,
            'fact_date' => $this->fact_date,
            'duplicated' => $this->duplicated,
            'category_id' => $this->category_id,
            'employees_id' => $this->employees_id,
            'project_id' => $this->project_id,
            'count_adress' => $this->count_adress,
            'fact_count_adress' => $this->fact_count_adress,
        ]);

        $query->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'comment_employees', $this->comment_employees])
            ->andFilterWhere(['like', 'comment_manager', $this->comment_manager]);

        return $dataProvider;
    }

    public function searchReportFact($params)
    {
        $query = (new Query())
            ->select(
                [
                    'routes.id as id',
                    'routes.town as town',
                    'routes.region as region',
                    'routes.status_route as status',
                    'routes.normal as circulation',
                    'routes.fact as factcirculation',
                    'routes.fact_count_address as factcountaddress',
                    'routes.count_adress as countaddress',
                    'CONCAT(routes.plane_start_date,"-",routes.plane_end_date) as routeperiod',
                    'CONCAT(routes.start_date,"-",routes.fact_date) as routeperiodfact',
                    'projects.name as projectname',
                    'projects.id as projectid',
                    'clients.name as clientname',
                    'employees.firstname as employees',
                ]
            )
            ->from('routes')
            ->leftJoin('projects','projects.id=routes.project_id')
            ->leftJoin('employees','employees.id=routes.employees_id')
            ->leftJoin('placement_method','placement_method.id = routes.placement_id')
            ->leftJoin('clients','clients.id = projects.client_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status_route' => $this->status_route,
            'start_date' => $this->start_date,
            'fact_date' => $this->fact_date,
            'duplicated' => $this->duplicated,
            'category_id' => $this->category_id,
            'employees_id' => $this->employees_id,
            'project_id' => $this->project_id,
            'count_adress' => $this->count_adress,
            'fact_count_adress' => $this->fact_count_adress,
        ]);

        $query->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'comment_employees', $this->comment_employees])
            ->andFilterWhere(['like', 'comment_manager', $this->comment_manager]);

        return $dataProvider;
    }
}
