<?php

namespace app\models;

use app\services\Polygon;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "routes".
 *
 * @property int $id
 * @property string $name
 * @property int $status_route Статус маршрута
 * @property int $normal Норма
 * @property string $start_date Дата начала
 * @property string $fact_date Дата завершения
 * @property string $video Видео файл
 * @property int $duplicated Дубликат
 * @property int $category_id Категория
 * @property int $employees_id Сотрудник
 * @property int $project_id Проект
 * @property string $comment_employees Комментарий промоутера
 * @property string $comment_manager Комментарий менеджера
 * @property int $count_adress Количество адресов
 * @property int $fact_count_adress Фактически выполнено
 * @property int $completed_count Подтвердженное кол-во
 * @property int $fines_sum Сумма штрафов
 * @property string $comment_leader Комментарий руководителя
 * @property string $comment_quality_manager Комментарий менеджера качества
 *
 * @property CommentRoutes[] $commentRoutes
 * @property RouteAddress[] $routeAddresses
 * @property CategoryCalculation $category
 * @property Employees $employees
 * @property Projects $project
 * @property RoutesPlacementMethod[] $routesPlacementMethods
 * @property PlacementMethod[] $placementMethods
 * @property RoutesAddressList[] $routesAddressLists
 * @property AddressList[] $addressLists
 */
class Routes extends \yii\db\ActiveRecord
{
    /**
     * @var string
     */
    public $intervalDates;

    /**
     * @var array
     */
    public $placementMethodsField;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'routes';
    }

    public static $colorStatus = ['#fff', '#236500', '#ffc301', '#c91a31'];

    const ROUTE_NEW = 0;
    const ROUTE_PLAN = 1;
    const ROUTE_WORK = 2;
    const ROUTE_CANCEL = 3;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_route', 'normal', 'duplicated', 'category_id', 'employees_id', 'project_id', 'count_adress', 'fact_count_adress', 'completed_count', 'fines_sum', 'zone_id'], 'integer'],
            [['intervalDates', 'start_date', 'fact_date', 'placement_id', 'maket'], 'safe'],
            [['comment_leader', 'comment_quality_manager'], 'string'],
            [['comment_employees', 'comment_manager',], 'safe'],
            [['name', 'video', 'town', 'region'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryCalculation::class, 'targetAttribute' => ['category_id' => 'id']],
            [['employees_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::class, 'targetAttribute' => ['employees_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::class, 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $zone = Zones::findOne($this->zone_id);
        if ($zone) {
            $this->name = $zone->name;
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->placementMethodsField != null) {
            RoutesPlacementMethod::deleteAll(['routes_id' => $this->id]);

            foreach ($this->placementMethodsField as $method) {
                (new RoutesPlacementMethod(['routes_id' => $this->id, 'placement_method_id' => $method]))->save(false);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'town' => 'Город',
            'region' => 'Район',
            'normal' => 'Норма',
            'status_route' => 'Статус маршрута',
            'start_date' => 'Дата начала',
            'fact_date' => 'Дата завершения',
            'intervalDates' => 'Дата начала — Дата завершения',
            'video' => 'Видео файл',
            'duplicated' => 'Дубликат',
            'category_id' => 'Категория',
            'employees_id' => 'Сотрудник',
            'project_id' => 'Проект',
            'comment_employees' => 'Комментарий промоутера',
            'comment_manager' => 'Комментарий менеджера',
            'count_adress' => 'Количество адресов',
            'fact_count_adress' => 'Фактически выполнено',
            'completed_count' => 'Подтвержденное кол-во',
            'fines_sum' => 'Сумма штрафов',
            'comment_leader' => 'Комментарий руководителя',
            'comment_quality_manager' => 'Комментарий менеджера качества',

            'placementMethodsField' => 'Типы работ',
            'zone_id' => 'Зона',
            'plane_start_date' => 'Планируемое начало',
            'plane_end_date' => 'Планируемое окончание',
            'placement_id' => 'Типы работ',
            'maket' => 'Макет',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommentRoutes()
    {
        return $this->hasMany(CommentRoutes::class, ['route_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRouteAddresses()
    {
        return $this->hasMany(RouteAddress::class, ['route_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CategoryCalculation::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees()
    {
        return $this->hasOne(Employees::class, ['id' => 'employees_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZone()
    {
        return $this->hasOne(Zones::class, ['id' => 'zone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::class, ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlacementMethod()
    {
        return $this->hasOne(PlacementMethod::class, ['id' => 'placement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoutesAddressLists()
    {
        return $this->hasMany(RoutesAddressList::class, ['routes_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressLists()
    {
        return $this->hasMany(AddressList::class, ['id' => 'address_list_id'])->viaTable('routes_address_list', ['routes_id' => 'id']);
    }

    public function getCurrentStatus()
    {
        switch ($this->status_route) {
            case self::ROUTE_NEW:
                return 'Новий';
            case self::ROUTE_PLAN:
                return 'Запланирован';
            case self::ROUTE_WORK:
                return 'В работе';
            case self::ROUTE_CANCEL:
                return 'Отказ';
            default:
                return "Неизвестно";
        }
    }

    public function beforeDelete()
    {
        $routeAddress = RouteAddress::deleteAll(['route_id' => $this->id]);
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }



    public static function getRouteFromEmployee($emloyee)
    {
        return ArrayHelper::map(static::find()->where(['employees_id' => $emloyee])->asArray()->all(),
            'id','name');
    }

    public function addRouteAddress()
    {
        $coords = Zones::find()->where(['id'=>$this->zone_id])->one();
        $coords = unserialize($coords->coorarr);
        $polygon = Polygon::factory($coords[0]);
        foreach ($polygon->getObjects() as $item){
            $routeAddress = new RouteAddress();
            $routeAddress->address_id = $item['id'];
            $routeAddress->route_id = $this->id;
            $routeAddress->save();
        }
    }

}
