<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employees_status".
 *
 * @property int $id
 * @property int $employees_id Сотрудник
 * @property int $status Статус
 * @property string $date_status Дата
 */
class EmployeesStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employees_id', 'status'], 'integer'],
            [['date_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employees_id' => 'Сотрудник',
            'status' => 'Статус',
            'date_status' => 'Дата',
        ];
    }
}
