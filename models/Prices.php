<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prices".
 *
 * @property int $id
 * @property string $name Назва
 * @property string $price Цена
 * @property string $price_type Тип цени
 */
class Prices extends \yii\db\ActiveRecord
{
    const TYPE_COUNT = 0;
    const TYPE_HOUR =1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'price_type'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'price' => 'Цена',
            'price_type' => 'Тип цены',
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        $types = [];
        $types[self::TYPE_COUNT] = 'За шт';
        $types[self::TYPE_HOUR] = 'За час';
        return $types;

    }

    /**
     * @param $role
     * @return string
     */
    public function getTypeName($type)
    {
        switch ($type) {
            case self::TYPE_COUNT: return "За шт";
            case self::TYPE_HOUR: return "За час";
            default: return "Неизвестно";
        }
    }

    /**
     * @return string
     */
    public function getCurrentType()
    {
        switch ($this->price_type) {
            case self::TYPE_COUNT: return "За шт";
            case self::TYPE_HOUR: return "За час";
            default: return "Неизвестно";
        }
    }

    public static  function getPriceListFullName()
    {
        $prices = static::find()->all();
        $priceList = [];
        foreach ($prices as $price) {
            $priceList[$price->id] = $price->name. " : " .$price->price ."/".static::getTypeName($price->price_type);
        }
        return $priceList;
    }

    public static  function getPriceByID($id)
    {
        return  static::findOne($id)->price;
    }
}
