<?php

namespace app\models;

use app\services\Polygon;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "route_address".
 *
 * @property int $id
 * @property int $address_id адрес
 * @property int $fine_id Штраф
 * @property int $route_id Маршрут
 * @property int $fine_percent Процент штрафа
 * @property int $fine_comment Комментарий штрафа
 * @property string $plane_work План
 * @property string $fact_work Факт
 * @property string $confirm_work Подтверждено
 *
 * @property AddressList $address
 * @property Routes $route
 */
class RouteAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'route_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address_id', 'fine_id', 'route_id', 'fine_percent', 'fine_comment'], 'integer'],
            [['plane_date','comment'],'safe'],
            [['plane_work', 'fact_work', 'confirm_work'], 'number'],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => AddressList::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => Routes::className(), 'targetAttribute' => ['route_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address_id' => 'адрес',
            'fine_id' => 'Штраф',
            'route_id' => 'Маршрут',
            'fine_percent' => 'Процент штрафа',
            'fine_comment' => 'Комментарий штрафа',
            'plane_work' => 'План',
            'fact_work' => 'Факт',
            'confirm_work' => 'Подтверждено',
            'plane_date' => 'Планируемая дата размещения',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(AddressList::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Routes::className(), ['id' => 'route_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        $route = Routes::findOne($this->route_id);
        $minDate = RouteAddress::find()->where(['route_id'=>$this->route_id])->min('plane_date');
        $maxDate = RouteAddress::find()->where(['route_id'=>$this->route_id])->max('plane_date');
        $countHouse = RouteAddress::find()->where(['route_id'=>$this->route_id])->count('id');
        $address = RouteAddress::find()->where(['route_id'=>$this->route_id])->all();
        $countApartament = 0;
        $countEntrance =0;
        foreach ($address as $a) {
            $countApartament += ProjectAddress::find()->where(['address_id' => $a->address_id])->one()->apartament;
            $countEntrance += ProjectAddress::find()->where(['address_id' => $a->address_id])->one()->entrance;
        }
        $route->plane_start_date = $minDate;
        $route->plane_end_date = $maxDate;
        $route->count_adress = $countHouse;
        $route->count_apartament = $countApartament;
        $route->count_entrance = $countEntrance;
        $route->save();
    }

    public static function getCenterRouteAddress($route_id)
    {
        $route = Routes::findOne($route_id);
        $coords = Zones::find()->where(['id'=>$route->zone_id])->one();
        $coords = unserialize($coords->coorarr);
        $polygon = Polygon::factory($coords[0]);
        return $polygon->getCenterZone();
    }

    public static function getRouteAddress($route_id)
    {
        $route = Routes::findOne($route_id);
        $routeAddres = ArrayHelper::map(RouteAddress::find()->where(['route_id'=>$route_id])->asArray()->all(),'id','address_id');
        $coords = Zones::find()->where(['id'=>$route->zone_id])->one();
        $coords = unserialize($coords->coorarr);
        $polygon = Polygon::factory($coords[0]);
        $address = $polygon->getObjects();
        foreach ($address as $key => $addr){
            if (in_array($addr['id'],$routeAddres)) {
                $address[$key]['inroute'] = true;
            } else {
                $address[$key]['inroute'] = false;
            }
        }
        $center = $polygon->getCenterZone();
        return ['address' => $address, 'center' => $center];
    }
}
